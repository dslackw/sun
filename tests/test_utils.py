import unittest
from pathlib import Path

from sun.utils import Fetch, Utilities


class TestUtilities(unittest.TestCase, Utilities):

    """Test utilities.

    Attributes:
        fetch (TYPE): Utilities class.
    """

    def setUp(self) -> None:
        super(Utilities, self).__init__()
        self.fetch = Fetch()

    def test_read_repo_text_file(self) -> None:
        """Test read remote text file.
        """
        self.assertGreater(len(self.read_repo_text_file(
            'https://mirrors.slackware.com/slackware/slackware64-15.0/ChangeLog.txt')), 10)

    def test_read_local_text_file(self) -> None:
        """Test read local text file.
        """
        self.assertGreater(len(self.read_local_text_file(
            Path('/var/lib/slackpkg/ChangeLog.txt'))), 10)

    def test_fetch_updates(self) -> None:
        """Test fetch updates.
        """
        self.assertEqual([], list(self.fetch.updates()))


if __name__ == '__main__':
    unittest.main()
