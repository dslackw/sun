import unittest

from sun.cli.tool import Tools


class TestTools(unittest.TestCase):

    """Test tools.

    Attributes:
        tools (TYPE): Class Tools.
    """

    def setUp(self) -> None:
        self.tools = Tools()

    def test_check_updates(self) -> None:
        """Test check updates.
        """
        self.assertEqual(('No news is good news!', []), self.tools.check_updates())

    def test_daemon_status(self) -> None:
        """Test daemon status.
        """
        self.assertEqual(True, self.tools.daemon_status())

    def test_daemon_process(self) -> None:
        """Test daemon process.
        """
        self.assertEqual(
            'FAILED [1]: SUN is already running',
            self.tools.daemon_process('start', 'Starting SUN daemon:  /usr/bin/sun-daemon'))


if __name__ == '__main__':
    unittest.main()
