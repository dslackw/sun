## About

Let's SUN!

SUN (Slackware Update Notifier) is a tray notification applet
and daemon for informing about package updates in Slackware.
It also serves as a CLI tool for monitoring upgraded packages.

SUN works by default with slackpkg, as well as with other tools
like slpkg. You can probably use SUN with other Slackware-based
Linux distributions as well.

## Documentation

https://dslackw.gitlab.io/sun/
